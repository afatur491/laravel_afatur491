$(document).ready(function () {
    $(document).on("click", ".btn-delete", function (e) {
        var _this = e.currentTarget;
        var url = $(_this).data("url");
        $("#delete-modal").modal("show");
        $("#delete-modal").find(".btn-action-delete").data("url", url);
    });
    $(".btn-action-delete").click(function () {
        var url = $(this).data("url");
        remove(url).then((data) => {
            $("#delete-modal").modal("hide");
            if (data.status == "success") {
                Swal.fire({
                    icon: "success",
                    title: data.message,
                    showConfirmButton: false,
                    timer: 1500,
                }).then(function () {
                    loadTable();
                });
            }
            if (data.status == "fail") {
                Swal.fire({
                    icon: "error",
                    title: data.message,
                    showConfirmButton: false,
                    timer: 1500,
                });
            }
        });
    });
    function remove(url) {
        var _token = $("meta[name=csrf]").attr("content");
        return $.ajax({
            url,
            method: "delete",
            dataType: "json",
            data: {
                _token,
            },
        });
    }

    loadTable();

    function loadTable() {
        $("tbody").html("");
        var url = $("meta[name=route-patient]").attr("content");
        $.ajax({
            url,
            dataType: "json",
            success: (data) => {
                var table = "";
                $.each(data.data, function (k, v) {
                    table += templateTable(v, k + 1);
                });
                $("tbody").html(table);
            },
        });
    }
    function templateTable(data, i) {
        return `<tr>
                        <td>${i}</td>
                        <td>${data.name}</td>
                        <td>${data.address}</td>
                        <td>${data.phone_number}</td>
                        <td>${data.hospital.name}</td>
                        <td>${templateAction(data.action)}</td>
                    </tr>`;
    }
    function templateAction(arr) {
        return `<a href="${arr["edit"]}" class="btn btn-warning btn-sm">Ubah</a>
                <button class="btn btn-danger btn-sm btn-delete" data-url="${arr["delete"]}">Hapus</button>
                `;
    }
});
