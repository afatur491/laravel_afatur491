$(document).ready(function () {
    $(".btn-delete").click(function () {
        var url = $(this).data("url");
        $("#delete-modal").modal("show");
        $("#delete-modal").find(".btn-action-delete").data("url", url);
    });
    $(".btn-action-delete").click(function () {
        var url = $(this).data("url");
        remove(url).then((data) => {
            $("#delete-modal").modal("hide");
            if (data.status == "success") {
                Swal.fire({
                    icon: "success",
                    title: data.message,
                    showConfirmButton: false,
                    timer: 1500,
                }).then(function () {
                    location.reload();
                });
            }
            if (data.status == "fail") {
                Swal.fire({
                    icon: "error",
                    title: data.message,
                    showConfirmButton: false,
                    timer: 1500,
                }).then(function () {
                    location.reload();
                });
            }
        });
    });

    function remove(url) {
        var _token = $("meta[name=csrf]").attr("content");
        return $.ajax({
            url,
            method: "delete",
            dataType: "json",
            data: {
                _token,
            },
        });
    }
});
