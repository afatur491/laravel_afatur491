<?php

namespace Database\Seeders;

use App\Models\Hospital;
use App\Models\Patient;
use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Patient::create([
            'name' => 'Siapa',
            'address' => 'Dimana',
            'phone_number' => '0883277237',
            'hospital_id' => Hospital::first()->id
        ]);
    }
}
