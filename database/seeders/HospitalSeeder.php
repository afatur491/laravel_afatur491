<?php

namespace Database\Seeders;

use App\Models\Hospital;
use Illuminate\Database\Seeder;

class HospitalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hospital::create([
            'name' => 'Rumah sakit',
            'address' => 'alamat',
            'email' => 'adad@ada',
            'phone_number' => '088223999237'
        ]);
    }
}
