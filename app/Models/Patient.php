<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['action'];

    public function hospital()
    {
        return $this->belongsTo(Hospital::class);
    }
    public function getActionAttribute()
    {
        return [
            'delete' => route('patient.destroy', ['patient' => $this->attributes['id']]),
            'edit' =>  route('patient.edit', ['patient' => $this->attributes['id']])
        ];
    }
}
