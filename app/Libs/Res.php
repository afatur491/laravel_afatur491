<?php

namespace App\Libs;

class Res
{
    public static function fail($message)
    {
        return response()->json(['status' => 'fail', 'message' => $message, 'data' => []]);
    }
    public static function success($data, $message = 'Berhasil melakukan aksi')
    {
        return response()->json(['status' => 'success', 'message' => $message, 'data' => $data]);
    }
}
