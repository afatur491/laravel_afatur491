<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }
    public function attempt(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $login = Auth::attempt($request->only('username', 'password'));

        if (!$login) return redirect()->back()->with('failed', 'Login gagal, silahkan periksa username dan password anda!');

        return redirect()->route('hospital.index')->with('success', 'Login Berhasil');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('auth.login')->with('success', 'Berhasil Logout');
    }
}
