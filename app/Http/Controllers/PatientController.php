<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatientRequest;
use App\Libs\Res;
use App\Models\Hospital;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $patients = Patient::with('hospital');
            if ($request->get('hospital')) $patients = $patients->where('hospital_id');
            return Res::success($patients->get());
        }
        return view('patient.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hospitals = Hospital::get();
        return view('patient.create', compact('hospitals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientRequest $request)
    {
        Patient::create($request->only('hospital_id', 'name', 'address', 'phone_number'));

        return redirect()->route('patient.index')->with('success', 'Pasien berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        $hospitals = Hospital::get();
        return view('patient.edit', compact('patient', 'hospitals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatientRequest $request, $id)
    {
        Patient::findOrFail($id);
        Patient::whereId($id)->update($request->only('hospital_id', 'name', 'address', 'phone_number'));

        return redirect()->route('patient.index')->with('success', 'Pasien berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hospital = Patient::whereId($id)->first();
        if (!$hospital) return Res::fail('Pasien tidak ditemukan');
        try {
            Patient::destroy($id);
            return Res::success($hospital, 'Berhasil menghapus pasien');
        } catch (\Throwable $th) {
            return Res::fail($th->getMessage());
        }
    }
}
