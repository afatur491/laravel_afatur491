<?php

namespace App\Http\Controllers;

use App\Http\Requests\HospitalRequest;
use App\Libs\Res;
use App\Models\Hospital;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hospitals = Hospital::all();
        return view('hospital.index', compact('hospitals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hospital.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HospitalRequest $request)
    {
        try {
            DB::beginTransaction();
            $hospital = Hospital::create($request->only('name', 'address', 'email', 'phone_number'));
            DB::commit();
            return redirect()->route('hospital.index')->with('success', 'Rumah sakit berhasil ditambahkan');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('failed', $th->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Hospital $hospital)
    {
        return view('hospital.edit', compact('hospital'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HospitalRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $hospital = Hospital::findOrFail($id);
            Hospital::whereId($id)->update($request->only('name', 'address', 'email', 'phone_number'));
            DB::commit();
            return redirect()->route('hospital.index')->with('success', 'Rumah sakit berhasil diubah');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('failed', $th->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hospital = Hospital::whereId($id)->first();
        if (!$hospital) return Res::fail('Rumah Sakit tidak ditemukan');
        try {
            Hospital::destroy($id);
            return Res::success($hospital, 'Berhasil menghapus rumah sakit');
        } catch (\Throwable $th) {
            return Res::fail($th->getMessage());
        }
    }
}
