<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:225',
            'phone_number' => 'required|digits_between:9,14',
            'address' => 'required',
            'hospital_id' => 'required|exists:hospitals,id'
        ];
    }
    public function attributes()
    {
        return [
            'name' => 'nama pasien',
            'hospital_id' => 'rumah sakit',
            'phone_number' => 'no telepon',
            'address' => 'alamat',
        ];
    }
}
