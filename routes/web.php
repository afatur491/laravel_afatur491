<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\PatientController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('hospital.index');
});

Route::middleware(['auth'])->group(function () {
    Route::resource('hospital', HospitalController::class)->except(['show']);
    Route::resource('patient', PatientController::class)->except(['show']);
});
Route::prefix('auth')->name('auth.')->middleware(['guest'])->group(function () {
    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/attempt', [AuthController::class, 'attempt'])->name('attempt');
});
Route::get('auth/logout', [AuthController::class, 'logout'])->name('auth.logout');
