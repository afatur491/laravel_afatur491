@extends('layout.main')

@section('content')
<x-table-title title="Rumah Sakit" create="{{route('hospital.create')}}"></x-table-title>

<div class="row">
    <div class="col-12">
        {{-- <div class="alert-container">
            @if (session()->has('failed'))
            <div class="alert alert-danger mt-4">
                {{session()->get('failed')}}
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success mt-4">
                {{session()->get('success')}}
            </div>
            @endif
        </div> --}}
        <div class="card">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Daftar Rumah Sakit</h6>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Nama Rumah Sakit</th>
                        <th>Alamat</th>
                        <th>Email</th>
                        <th>Telepon</th>
                        <th>Aksi</th>
                    </tr>
                    @foreach ($hospitals as $h => $hospital)
                    <tr>
                        <td>{{$h+1}}</td>
                        <td>{{ $hospital->name }}</td>
                        <td>{{ $hospital->address }}</td>
                        <td>{{ $hospital->email }}</td>
                        <td>{{ $hospital->phone_number }}</td>
                        <td>
                            <x-action-button delete="{{route('hospital.destroy',['hospital' => $hospital->id])}}" edit="{{route('hospital.edit',['hospital' => $hospital->id])}}" ></x-action-button>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{asset('js/hospital.js')}}"></script>
@if (session()->has('failed'))
<script>
    Swal.fire({
        icon: "error",
        title: "{{session()->get('failed')}}",
        showConfirmButton: false,
        timer: 1500,
        });
</script>
@endif
@if (session()->has('success'))
<script>
    Swal.fire({
        icon: "success",
        title: "{{session()->get('success')}}",
        showConfirmButton: false,
        timer: 1500,
        });
</script>
@endif
@endsection

