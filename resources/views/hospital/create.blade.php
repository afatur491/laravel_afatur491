@extends('layout.main')
@section('content')

<div class="row">
    <div class="col-12">
        @if (session()->has('failed'))
        <div class="alert alert-danger my-4">
            {{session()->get('failed')}}
        </div>
        @endif
        <div class="card">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Tambah Rumah Sakit</h6>
            </div>
            <div class="card-body">
                <form action="{{route('hospital.store')}}" method="POST" class="row">
                    @csrf
                    <div class="col-4">
                        <div class="form-group">
                            <label for="name">Nama Rumah Sakit</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old('name') ?? ''}}">
                            @error('name')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{old('email') ?? ''}}">
                            @error('email')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="phone_number">Telepon</label>
                            <input type="phone_number" class="form-control" id="phone_number" name="phone_number" value="{{old('phone_number') ?? ''}}">
                            @error('phone_number')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <textarea name="address" id="address" class="form-control" rows="3">{{old('address') ?? ''}}</textarea>
                            @error('address')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <hr>
                        <button class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
