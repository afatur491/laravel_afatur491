@extends('layout.auth')
@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-8 col-lg-8 col-md-9">
            @if (session()->has('failed'))
            <div class="alert alert-danger mt-4">
                {{session()->get('failed')}}
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success mt-4">
                {{session()->get('success')}}
            </div>
            @endif
            <div class="card o-hidden border-0 shadow-lg my-4">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Login</h1>
                                </div>
                                <form class="user" method="POST" action="{{route('auth.attempt')}}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user"
                                             placeholder="Masukkan Username" name="username">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user"
                                            placeholder="Password" name="password">
                                    </div>
                                    <button class="btn btn-primary btn-user btn-block">
                                        Login
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
