@extends('layout.main')

@section('meta')
<meta name="route-patient" content="{{route('patient.index')}}">
@endsection

@section('content')
<x-table-title title="Pasien" create="{{route('patient.create')}}"></x-table-title>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Daftar Pasien</h6>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pasien</th>
                            <th>Alamat</th>
                            <th>No Telepon</th>
                            <th>Rumah Sakit</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{asset('js/patient.js')}}"></script>
@if (session()->has('failed'))
<script>
    Swal.fire({
        icon: "error",
        title: "{{session()->get('failed')}}",
        showConfirmButton: false,
        timer: 1500,
        });
</script>
@endif
@if (session()->has('success'))
<script>
    Swal.fire({
        icon: "success",
        title: "{{session()->get('success')}}",
        showConfirmButton: false,
        timer: 1500,
        });
</script>
@endif
@endsection

