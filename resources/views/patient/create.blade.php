@extends('layout.main')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Tambah Pasien</h6>
            </div>
            <div class="card-body">
                <form action="{{route('patient.store')}}" method="POST" class="row">
                    @csrf
                    <div class="col-4">
                        <div class="form-group">
                            <label for="name">Nama Pasien</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old('name') ?? ''}}">
                            @error('name')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="phone_number">No Telepon</label>
                            <input type="phone_number" class="form-control" id="phone_number" name="phone_number" value="{{old('phone_number') ?? ''}}">
                            @error('phone_number')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="phone_number">Rumah Sakit</label>
                            <select class="form-control" name="hospital_id">
                                <option selected disabled>Pilih Rumah Sakit</option>
                                @foreach ($hospitals as $hospital)
                                <option value="{{$hospital->id}}" {{old('hospital_id') == $hospital->id ? 'selected' : ''}}>{{$hospital->name}}</option>
                                @endforeach
                            </select>
                            @error('hospital_id')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <textarea name="address" id="address" class="form-control" rows="3">{{old('address') ?? ''}}</textarea>
                            @error('address')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <hr>
                        <button class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    @if (session()->has('failed'))
    <script>
        Swal.fire({
        icon: "error",
        title: "{{session()->get('failed')}}",
        showConfirmButton: false,
        timer: 1500,
        });
    </script>
    @endif
    @if (session()->has('success'))
    <script>
        Swal.fire({
        icon: "success",
        title: "{{session()->get('success')}}",
        showConfirmButton: false,
        timer: 1500,
        });
    </script>
    @endif
@endsection
